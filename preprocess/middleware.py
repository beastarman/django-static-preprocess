from django.conf import settings
from . import compiler

class Recompile:
	def process_request(self,request):
		if(settings.DEBUG):
			cache = request.META.get("HTTP_CACHE_CONTROL","")
			if(cache == "no-cache"):
				compiler.preprocess()
