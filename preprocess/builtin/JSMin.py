from jsmin import jsmin
from ..compiler import BaseCompiler

class Compiler(BaseCompiler):
	def __init__(self):
		super(Compiler, self).__init__()
		self.target_extension = "js"
	
	def compile(self):
		return jsmin(self.src)
