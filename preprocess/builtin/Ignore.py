from ..compiler import BaseCompiler
from ..exceptions import DoNotCompile

class Compiler(BaseCompiler):
	def __init__(self):
		super(Compiler, self).__init__()
	
	def compile(self):
		raise DoNotCompile()
