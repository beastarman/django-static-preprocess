from ..compiler import BaseCompiler

class Compiler(BaseCompiler):
	def __init__(self):
		super(Compiler, self).__init__()
	
	def compile(self):
		return self.src
