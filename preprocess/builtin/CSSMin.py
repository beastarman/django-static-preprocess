from csscompressor import compress
from ..compiler import BaseCompiler

class Compiler(BaseCompiler):
	def __init__(self):
		super(Compiler, self).__init__()
		self.target_extension = "css"
	
	def compile(self):
		return compress(self.src)
