from django.template import Template,Context
from django.conf import settings
from ..compiler import BaseCompiler

class Compiler(BaseCompiler):
	def __init__(self):
		super(Compiler, self).__init__()
	
	def compile(self):
		ctx = {"settings": settings}
		ctx.update(self.update_context())
		
		template_dirs = settings.TEMPLATE_DIRS
		settings.TEMPLATE_DIRS = template_dirs + (self.base_dir,)
		
		rendered = Template(self.src).render(Context(ctx))
		
		settings.TEMPLATE_DIRS = template_dirs
		return rendered
	
	def update_context(self):
		return {}
