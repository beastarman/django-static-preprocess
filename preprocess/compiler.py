from importlib import import_module
from django.conf import settings

import fnmatch, re, os, copy

from .exceptions import DoNotCompile

class BaseCompiler(object):
	def __init__(self):
		self.target_extension = None
	
	def create(self,src,base_dir,file_path):
		self.src = src
		self.base_dir = base_dir
		self.file_path = file_path
	
	def compile(self):
		return self.src

class MultiCompiler(BaseCompiler):
	def __init__(self,compilers):
		super(MultiCompiler, self).__init__()
		self.compilers = []
		for i in compilers:
			self.compilers.append(i)
			if(i.target_extension):
				self.target_extension = i.target_extension
	
	def compile(self):
		for i in self.compilers:
			i.create(self.src,self.base_dir,self.file_path)
			self.src = i.compile()
		return self.src

def compile_in_tree(source,target,compilers,base_source=None):
	if(base_source == None):
		base_source = source
	for d in os.listdir(source):
		path = os.path.join(source,d)
		if(os.path.isfile(path)):
			do_not_compile=False
			found=False
			
			for compiler in compilers:
				for r in compiler['file']:
					if(r.match(d)):
						found=True
						f = open(path)
						try:
							content = compiler["compiler"].create(f.read(),base_source,path)
							content = compiler["compiler"].compile()
						except DoNotCompile:
							do_not_compile=True
						else:
							extension  = compiler.get('extension',compiler["compiler"].target_extension)
							
							if(extension):
								destiny = d.split(".")
								destiny = ("%s.%s" % (destiny[0],extension)) if len(destiny)==1 else ("%s.%s" % (".".join(destiny[:-1]),extension))
							else:
								destiny = d
						finally:
							f.close()
					if(found):
						break
				if(found):
					break
			if(do_not_compile or not found):
				continue
			
			destiny = os.path.join(target,destiny)
			
			try:
				os.makedirs(os.path.dirname(destiny))
			except:
				pass
			
			f = open(destiny,"w")
			
			if(isinstance(content,unicode)):
				content = content.encode("UTF-8")
			
			f.write(content)
			f.close()
		else:
			compile_in_tree(path,os.path.join(target,d),compilers,base_source)

def preprocess():
	for compiler_settings in copy.deepcopy(settings.PREPROCESSOR_COMPILERS):
		if(isinstance(compiler_settings['compilers'],dict)):
			compiler_settings['compilers'] = (compiler_settings['compilers'],)
		
		compiler_settings['compilers'] = list(compiler_settings['compilers'])
		
		compiler_settings['compilers'].append({
			'file': ["*"],
			'compiler': getattr(settings,'PREPROCESSOR_DEFAULT_COMPILER',["preprocess.builtin.Nothing"]),
		})
		
		for compiler in compiler_settings['compilers']:
			if('file' not in compiler):
				compiler['file'] = ('*',)
			if(isinstance(compiler['file'],str)):
				compiler['file'] = (compiler['file'],)
			
			if(isinstance(compiler['compiler'],str)):
				compiler['compiler'] = (compiler['compiler'],)
			
			compiler['file'] = list(compiler['file'])
			
			for f in xrange(len(compiler['file'])):
				compiler['file'][f] = re.compile(fnmatch.translate(compiler['file'][f]))
			
			compiler_list = []
			for compiler_i in compiler['compiler']:
				module = __import__(compiler_i)
				for m in compiler_i.split(".")[1:]:
					module = getattr(module,m)
				compiler_list.append(module.Compiler())
			compiler['compiler'] = MultiCompiler(compiler_list)
		
		compile_in_tree(
			os.path.join(settings.BASE_DIR,compiler_settings["source"]),
			os.path.join(settings.BASE_DIR,compiler_settings["target"]),
			compiler_settings['compilers']
		)
		
