import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-static-preprocess',
    version='0.6',
    packages=['preprocess'],
    include_package_data=True,
    license='BSD License',
    description='A simple Django app to preprocess static files.',
    long_description=README,
    url='https://bitbucket.org/beastarman/django-static-preprocess/',
    author='Douglas Vanny Bento',
    author_email='douglas.vanny@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
		'django',
		'csscompressor',
		'jsmin',
    ],
)
