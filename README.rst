========================
Django Static Preprocess
========================

This app was created to allow files to be preprocessed before being made availabe

This can be used on varous cases, such as minifying CSS files.

Quick start
-----------

1. Add "preprocess" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'preprocess',
    )

2. Create a PREPROCESSOR_COMPILERS tuple setting in the format::

    {
        'source': <source_folder>,
        'target': <target_folder>,
        'compilers': (
            {
                'file': (
                    <filename_match>*
                ),
                'compiler': (
                    <compiler>*
                ),
                'extension': <extension>?
            }*
        )
    }

  like in the following example::
    
    PREPROCESSOR_COMPILERS = (
        {
            'source': "resources/prep/js/",
            'target': "resources/static/js/",
            'compilers': (
                {
                    'file': (
                        "*.min.js",
                    ),
                    'compiler': (
                        "preprocess.builtin.Nothing",
                    ),
                },
                {
                    'extension': "js",
                    'file': (
                        "*.js",
                        "*.javascript",
                    ),
                    'compiler': (
                        "preprocess.builtin.DjangoTemplate",
                        "preprocess.builtin.JSMin",
                    ),
                },
            ),
        },
    )

  There are already some built-in compilers available for use. They are:

    * preprocess.builtin.CSSMin - CSS minifier
    * preprocess.builtin.JSMin - Javascript/ECMAScript minifier
    * preprocess.builtin.DjangoTemplate - Interpret file as if it were an Django template
    * preprocess.builtin.Nothing - Outputs file content without touching it
    * preprocess.builtin.Ignore - Ignores file, not creating anything on the target folder

  When executed, the script will loop through every dict in the PREPROCESSOR_COMPILERS[]["compilers"] and will use only the first compiler where the filename matches the one informed on its "file" parameter. In the example above, it will minify every file ending in ".js" or ".javascript", but if it ends in ".min.js", nothing will be done.

3. To define the default compiler when a file isn't matched on any case, define PREPROCESSOR_DEFAULT_COMPILER in your settings file with the compiler you want to use in such case. It can be a string or tuple/list. The default default is "preprocess.builtin.Nothing".

4. To allow automatic compilation of your files, add "preprocess.middleware.Recompile" to your MIDDLEWARE_CLASSES setting::
    
    MIDDLEWARE_CLASSES = (
        ...
        'preprocess.middleware.Recompile',
    )

  By doing this, every time you run django, all files will be compiled. If you have DEBUG = True, every request with HTTP_CACHE_CONTROL = "no-cache" will recompile everything (Ctrl+Shift+R on some mayor browsers).
